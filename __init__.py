# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase
from . import product


def register():
    Pool.register(
        product.Product,
        product.Template,
        product.AddSupplierToProductsStart,
        purchase.CreatePurchaseSuggestedStart,
        purchase.CreatePurchaseRequestBySupplierStart,
        purchase.CreateRequestBySupplierStart,
        product.UpdateCostPriceStart,
        purchase.PurchaseSuggestedReportStart,
        module='purchase_suggested', type_='model')
    Pool.register(
        product.AddSupplierToProducts,
        purchase.CreatePurchaseSuggested,
        product.UpdateProductBySupplier,
        purchase.CreatePurchaseRequestBySupplier,
        purchase.CreateRequestBySupplier,
        product.UpdateCostPrice,
        purchase.PrintPurchaseSuggestedReport,
        module='purchase_suggested', type_='wizard')
    Pool.register(
        purchase.PurchaseSuggestedReport,
        purchase.PurchaseRequisitionReport,
        module='purchase_suggested', type_='report')
